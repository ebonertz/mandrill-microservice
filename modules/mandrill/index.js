'use strict';
var Promise = require('bluebird');

module.exports = function (app) {
  var config = app.config;
  var module = app.mandrillService = {};
  var logger = app.logger;

  var mandrill = require('mandrill-api/mandrill'),
  mandrill_client = new mandrill.Mandrill(config.get("mandrill").key);
}

module.send_one = function (options) {
    return new Promise(function (resolve, reject) {
      if (!options.email) {
        logger.error("No email address to send to");
        return reject("No email address to send to");
      }

      var to = {
        email: options.email,
        name: options.name,
        type: "to"
      }

      var message = {
        to: [to],
        from_email: config.get("mandrill").options ? config.get("mandrill").options.from_email : null,
        from_name: config.get("mandrill").options ? config.get("mandrill").options.from_name : null,
        subject: options.subject || null,
        merge_language: "handlebars",
        merge: true,
        global_merge_vars: options.template_content || [],
        attachments: options.attachments || null
      }

      mandrill_client.messages.sendTemplate({
          "template_name": options.template,
          "template_content": options.template_content || [],
          "message": message,
          "async": true
        },
        function (result) {
          if (result[0].status === "rejected") {
            return reject("The email is rejected by Mandrill, Reason: " + result[0].reject_reason);
          } else {
            return resolve(result)
          }
        }, function (e) {
          return reject(e);
        });
    })
  };
