var SphereClient = require('sphere-node-sdk').SphereClient;
var _ = require('lodash');

module.exports = function (app) {
  var config = app.config;
  var module = app.ctService = {};
  var client = new SphereClient({
    config: app.config.get("sphereProjectConfig"),
    host: app.config.get("sphereProjectConfig:api_host"),
    oauth_host: app.config.get("sphereProjectConfig:oauth_url")
  });
  var logger = app.logger;

  module.getMessages = function (date, types, expand) {
    if (expand) {
      return client.messages
        .where(`createdAt > "${date}"`)
        .where(`type in (${_.join(_.map(types, function (type) {
          return '"' + type + '"';
        }), ",")})`)
        .expand(expand)
        .all()
        .fetch()
        .then(function (res) {
          return res.body.results;
        }).catch(function (err) {
          logger.error("Error getting messages for types: %s, Error: %s", types, err);
        })
    } else {
      return client.messages
        .where(`createdAt > "${date}"`)
        .where(`type in (${_.join(_.map(types, function (type) {
          return '"' + type + '"';
        }), ",")})`)
        .all()
        .fetch()
        .then(function (res) {
          return res.body.results;
        }).catch(function (err) {
          logger.error("Error getting messages for types: %s, Error: %s", types, err);
        })
    }

  };


  module.getOrdersCreatedMessages = function (date) {
    return module.getMessages(date, ["OrderCreated", "OrderImported"]);
  };

  module.getCustomersCreatedMessages = function (date) {
    return module.getMessages(date, ["CustomerCreated"]);
  };

  module.getDeliveryAddedMessages = function (date) {
    return module.getMessages(date, ["ParcelAddedToDelivery"], "resource");
  };

  module.getOrdersInvoicedMessages = function (date) {
    return module.getMessages(date, ["OrderStateChanged"], "resource").then(function (messages) {
      var filterMessages = _.filter(messages, function (message) {
        return message.orderState == "Complete";
      });
      return filterMessages;
    }).catch(function (err) {
      logger.error("Error getting messages for orders with state 'Complete', Error: %s", err);
    });
  };

  module.getRefundMessages = function (date) {
    return client.messages
      .where(`createdAt > "${date}"`)
      .where(`type = "PaymentTransactionAdded"`)
      .all()
      .fetch()
      .then(function (res) {
        return res.body.results;
      }).then(function (messages) {
        var filterMessages = _.filter(messages, function (message) {
          return message.transaction.type == "Refund";
        });
        return filterMessages;
      }).catch(function (err) {
        logger.error("Error getting messages for refund types, Error: %s", err);
      })
  };

  module.getPaymentCapturedMessages = function (date) {
    return client.messages
      .where(`createdAt > "${date}"`)
      .where(`type = "PaymentTransactionAdded"`)
      .all()
      .fetch()
      .then(function (res) {
        return res.body.results;
      }).then(function (messages) {
        var filterMessages = _.filter(messages, function (message) {
          return message.transaction.type == "Charge";
        });
        return filterMessages;
      }).catch(function (err) {
        logger.error("Error getting messages for payment captured types, Error: %s", err);
      })
  };

  module.getOrderByPaymentId = function (paymentId) {
    return client.orders.where(`paymentInfo(payments(id = "${paymentId}"))`).fetch().then(function (res) {
      if (!_.isEmpty(res.body.results)) {
        return res.body.results[0];
      }
    })
  }

  module.getCustomerById = function (customerId) {
    return client.customers.byId(customerId).fetch().then(function (res) {
      return res.body;
    });
  }


  module.getOrderById = function (orderId) {
    return client.orders.byId(orderId)
      .expand("paymentInfo.payments[*]")
      .expand("syncInfo[*].channel")
      .fetch().then(function (res) {
        return res.body;
      })
  };

  module.getLastMessageProcessed = function (serviceName) {
    return client.customObjects
      .where(`key="${serviceName}LastMessageProcessed"`)
      .fetch()
      .then(function (res) {
        return res.body.results
      }).then(function (results) {
        if (!_.isEmpty(results)) {
          return _.first(results).value;
        }
      });
  };

  module.setLastMessageProcessed = function (datetime, serviceName) {
    return client.customObjects.save({
      container: `${serviceName}LastMessageProcessed`,
      key: `${serviceName}LastMessageProcessed`,
      value: datetime.toISOString()
    })
  };


  return module;
}
