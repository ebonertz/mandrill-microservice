var app = {};
var Promise = require('bluebird');
var moment = require('moment');
var _ =require('lodash');

// Configuration object
require('./config')(app, [
    process.env.EXTERNAL_CONFIG
    , __dirname + '/config/' + (process.env.NODE_ENV || 'local') + '.json'
    , __dirname + '/config/defaults.json'
]);


require('./modules/ct-service')(app);
require('./modules/mandrill')(app);

var concurrency = app.config.get("concurrency");
var executionDatetime;
var transactionalEmails = app.config.get("mandrill:transactionalEmail");

if (_.isEmpty(transactionalEmails)) {
  setTimeout(function () {
    logger.error("Please set the transactional emails config in the configuration file");
    process.exit(1);
  }, 1000);
} else if (!serviceName) {
  setTimeout(function () {
    logger.error("Please set the service name in the configuration file");
    process.exit(1);
  }, 1000);
} else {
  app.ctService.getLastMessageProcessed(serviceName).then(function (lastMessageProcessedDate) {
    logger.info("Last message processed date: %s", lastMessageProcessedDate);
    if (lastMessageProcessedDate) {
      if(lastMessageProcessedDate > app.config.get("getMessagesFrom")){
        return lastMessageProcessedDate;
      }else{
        return app.config.get("getMessagesFrom") || moment().subtract(7, 'days').toISOString();
      }
    } else {
      return app.config.get("getMessagesFrom") || moment().subtract(7, 'days').toISOString();
    }
  }).then(function (fromDate) {
    executionDatetime = new Date();
    logger.info("Getting messages from date: %s", fromDate);

    var processes = [];

    _.each(transactionalEmails, function (transactionalEmail) {
      if (transactionalEmail.isActive) {
        switch (transactionalEmail.name) {
          case "welcome":
            processes.push(sendWelcomeEmails(transactionalEmail, fromDate));
          }
      }
    });      

var sendWelcomeEmails = function (transactionalEmail, date) {
  if (!transactionalEmail.template) {
    logger.error("Template not set for the transactional email. %s", transactionalEmail.name);
  } else {
    return app.ctService.getCustomersCreatedMessages(date).then(function (messages) {
      return Promise.map(messages, function (message) {
        var customer = message.customer;
        customer.password = "";
        var options = {
          email: customer.email, // customer.email,
          template: transactionalEmail.template,
          template_content: _.map(transactionalEmail.templateContent, function (field) {
            return {
              name: field.name,
              content: field.value
            }
          }).concat({
            name: "customerEmailAddress",
            content: customer.email
          }).concat({
            name: "customerfirstName",
            content: customer.firstName
          })
        };
        //logger.info("Options: %s", JSON.stringify(options, null, 2));
        logger.debug("Sending welcome transactional email to: %s", message.customer.email);
        return app.mandrillService.send_one(options).then(function () {
          logger.debug("Successfully welcome transactional email sent to: %s", message.customer.email);
        }).catch(function (err) {
          logger.error("Error sending welcome email for: %s. Error: %s", message.customer.email, err);
        })
      }, {concurrency: concurrency}).then(function () {
        return messages;
      }).catch(function (err) {
        logger.error("Error sending welcome email. Error: %s", err);
      })

    })
  }
};
module.exports = app;
